extern crate reqwest;
extern crate scraper;

extern crate serde;
extern crate serde_json;

use std::fs::File;
use std::io;
use std::io::prelude::*;

use scraper::{Html, Selector};

fn read_html_file(filename: &str) -> io::Result<()> {
    let mut file = File::open(&filename)?;
    let mut contents = String::new();

    file.read_to_string(&mut contents)
        .expect("could not read file content to string");

    parse_html_content(&contents);
    Ok(())
}

fn parse_html_content(contents: &str) {
    let parsed_html = Html::parse_document(&contents);
    let start_times = Selector::parse(".start-time").unwrap();

    for start_time in parsed_html.select(&start_times) {
        // Collects the start times into a vector
        let start_time_text = start_time.text().collect::<Vec<_>>();
        println!("{:?}", start_time_text);
    }
}

fn main() {
    read_html_file("./gdq.html").expect("could not read file");
    //rocket().launch();
}

/*
How to specify the format of a route
https://rocket.rs/v0.4/guide/requests/#optional-parameters

Use a struct to prepare the JSON to be sent to frontend
https://github.com/SergioBenitez/Rocket/blob/v0.4/examples/json/src/main.rs
#[get("/user/<id>", format = "json")]
fn user(id: usize) -> Json<User> { ... }
*/
